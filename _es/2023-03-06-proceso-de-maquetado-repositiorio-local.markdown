---
title: 'Proceso de Maquetado: repositiorio local'
description: Configuración del repositorio local
author:
- Sutty
post: 045bbe6b-a656-435b-ad9e-ca022ccc112c
categories: []
tags: []
locales: []
draft: false
order: 7
layout: post
uuid: bbdbbb9e-0116-4dea-bf3b-dbb2f7ad978b
liquid: false
usuaries:
- 27
last_modified_at: 2023-07-12 18:09:56.400785651 +00:00
---

<h1 style="text-align:start" id="clonar-sitio-nuevo-de-sutty">Clonar sitio nuevo de Sutty</h1><p style="text-align:start">En sutty tenemos un directorio dedicado a los respositorios donde esta hain.sh : <code>cd ~/proyectos/sutty/</code></p><ol><li><p style="text-align:start"><code>git clone $sitio-jekyll-theme.git </code>Entramos dentro del repositorio <code>cd $sitio-jekyll-theme.git</code></p></li></ol><h1 style="text-align:start" id="sutty-base-2.0">sutty-base 2.0</h1><p style="text-align:start">En la version 2.0 de sutty base usamos 2 herramientas nuevas. `task` o `go-task` para instalar y gestionar las flags de compilación, instalación de gemas. con `go-task --list` muestra las tareas disponibles a ejecutar.</p><p style="text-align:start">Dentro del repositorio:</p><p style="text-align:start">1.  <code>task bundle</code> para instalar dependencias de jekyll</p><p style="text-align:start">2. <code>task pnpm</code> para instalar dependencias de esbuild (js)</p><p style="text-align:start">3. <code>task build</code> para construir el sitio y publicarlo en <code>https://$sitio.sutty.local:4000</code></p><p style="text-align:start"></p><h1 style="text-align:start" id="sutty-base-1.0">sutty-base 1.0</h1><h2 style="text-align:start" id="instalamos-dependencias-con">instalamos dependencias con <code>make $flag</code>
</h2><ol start="2">
<li><p style="text-align:start">make bundle (instalar gemas)</p></li>
<li><p style="text-align:start">make yarn (instalar node y bootstrap)</p></li>
</ol><h2 style="text-align:start" id="construimos-el-sitio-y-lo-servimos">construimos el sitio y lo servimos</h2><ol start="4">
<li><p style="text-align:start">make build (construir el sitio)</p></li>
<li><p style="text-align:start">make serve servir el sitio en <code>$sitio.sutty.local:4000</code></p></li>
<li><p style="text-align:start">make webpack-dev-server (correr webpack para ejecutar el servidor de javascript local)</p></li>
</ol>