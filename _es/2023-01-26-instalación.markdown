---
title: instalación
description: post de instalacion del entrorno de trabajo de sutty
author: []
post: 045bbe6b-a656-435b-ad9e-ca022ccc112c
categories: []
tags: []
draft: false
order: 5
layout: post
uuid: 004b51ee-352e-4b30-b4c2-d343f926ec0d
liquid: false
usuaries:
- 27
last_modified_at: 2023-02-21 04:11:34.134336896 +00:00
---

# Sutty/Entorno Memoria del taller de maquetado 

Temario:

- Creación de llave SSH
- Cuentas de 0xacab.org, agregar llave SSH
- Configuración de git (nombre y mail)
- Instalar entorno de Sutty
- Analizar referencia de diseño
- Generar estructura de datos

## Cuentas de 0xacab, creación de llave SSH, agregar llave SSH
Todos los repositorios de Sutty están en https://0xacab.org/sutty/ Crear cuenta en 
0xacab (no se pueden usar mails de gmail)
En la terminal: `ssh-keygen -t ed25519` `cat ~/.ssh/id_ed25519.pub` Copiarla y 
pegar en: https://0xacab.org/profile/keys 

Configuración de git (nombre y mail)o-de-trabajo/instalacion.md</p>