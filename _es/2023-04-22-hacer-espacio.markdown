---
title: hacer espacio
description: a veces necesitamos más espacio en athshe!
author:
- Nulo
post: bb426f56-023f-4e66-afdc-4ab5fde46d95
categories: []
tags: []
draft: false
order: 10
layout: post
uuid: 138064ab-982f-44a5-a449-a8035e6012b5
liquid: false
usuaries:
- 417
last_modified_at: 2023-04-22 14:24:34.413140294 +00:00
---

<h2 style="text-align:start" id="borrar-gemas-de-sitios">Borrar gemas de sitios</h2><p style="text-align:start">estas se regeneran cuando se intentan usar los sitios, así que se pueden borrar si se necesita espacio temporalmente.</p><code>cd /srv/sutty/srv/http/data/_storage
# acá, la carpeta `gems` tiene las gemas de los sitios
rm -r gems</code><p style="text-align:start">en el futuro estas se van a limpiar automáticamente mensualmente.</p><h2 style="text-align:start" id="sacar-logs-de-sitios">Sacar logs de sitios</h2><p style="text-align:start">los logs de los sitios a veces pesan bastante. son <code>/srv/sutty/srv/http/data/_storage/*.psql.gz</code>. para ver el espacio que ocupan, podés usar <code>du -hc /srv/sutty/srv/http/data/_storage/*.psql.gz</code>.</p>